// SAMPO! Muutin t�n tallentamaan periodit millisekunteina!

function createPeriod(){

	console.log("Aloitetaan datan l�hetys");
	
	var lista = $('#wagePeriod').serializeArray();
	console.log("formi serialisoitu");
	
	var olio = {};
	console.log("tyhj� olio luotu");
	
	var firstDate = new XDate(
							parseInt( lista[2].value ),
							parseInt( lista[1].value ) - 1,
							parseInt( lista[0].value ) );
							
	var firstDateInt = firstDate.getTime();
	console.log("Ensimmainen pvm: " + firstDate.toString());
	console.log("Ensimmainen pvm millisekunteina: " + firstDateInt);
	
	// Asetetaan ensimm�inen p�iv�.
	olio["firstDate"] = firstDateInt;
	
	// Asetetaan viimeinen p�iv�.
	var lastDate = firstDate.addDays( parseInt( lista[3].value ) );
	
	console.log("Viimeinen pvm string 1: " + lastDate.toString() );
	console.log("V�hennet��n sekunti.");
	lastDate = lastDate.addSeconds( -1 );
	console.log("V�hennyksen j�lkeen: " + lastDate.toString() );
	
	var lastDateInt = lastDate.getTime();
	
	olio["lastDate"] = lastDateInt;
	console.log("Viimeinen pvm millisekunteina: " + lastDateInt);
	
	olio["type"] = "wagePeriod";
	console.log("dokumentin tyypiksi asetettu wagePeriod");
	
	$.ajax({
		type: 'POST',
		dataType: 'json',
		contentType: "application/json",
		data: JSON.stringify(olio),
		url: "http://54.228.200.239/sansio/",
		success: function(paluuarvo){
			console.log("paluuarvo:");
			console.log(paluuarvo);
		}
	});
};