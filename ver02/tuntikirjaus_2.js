var FormChanged = false;

$(document).ready(function() {
	checkLogin();
	console.log("document ready");
	getClients();
	//showUser();
});

function showSavedHours(date){
	$.mobile.showPageLoadingMsg();
	$.ajax({
        dataType: 'json',
 		contentType: "application/json",
 		url: "http://54.228.200.239/sansio/_design/views/_view/hourSlipsByUser?key=\"" + sessionStorage.getItem('username') + "\"",
 		success: function(sdata){
			console.log("data received");
     		console.log(sdata);
			getSavedHours(sdata,date);
        }
    });
};

function getSavedHours(users,date){
	var max = users["rows"].length;
	var hours = 0;
	var temp = new XDate(date);
	for (var i = 0 ; i < max ; i++)
	{
		if (users["rows"][i]["value"].date == temp){
			hours = hours + users["rows"][i]["value"].basicHours + users["rows"][i]["value"].KTA1Hours + users["rows"][i]["value"].KTA2Hours;
		}
	}	
	$('#thisDay').replaceWith("<div id=\"thisDay\">Tunteja merkattuna t&auml;lle p&auml;iv&auml;lle: " + hours + "</div>");
	$('#typeOfSlip').show();
	$.mobile.hidePageLoadingMsg();
};

function dialogInput(input){
	if (input == "pekkanen"){
		$('#typeOfLeave').replaceWith("<div id=\"typeOfLeave\">pekkasp&auml;iv&auml;n</div>");
		$('#yesButton').replaceWith("<div id=\"yesButton\"><a href=\"#\" data-role=\"button\" data-icon=\"check\" onclick=\"dialogOutput('pekkanen',8);return true\">Kyll&auml;</a></div>");
	}
	else if (input == "puolipekkanen"){
		$('#typeOfLeave').replaceWith("<div id=\"typeOfLeave\">puolipekkasen</div>");
		$('#yesButton').replaceWith("<div id=\"yesButton\"><a href=\"#\" data-role=\"button\" data-icon=\"check\" onclick=\"dialogOutput('puolipekkanen',4);return true\">Kyll&auml;</a></div>");
	}
	else if (input == "arkipyha"){
		$('#typeOfLeave').replaceWith("<div id=\"typeOfLeave\">arkipyh&auml;n</div>");
		$('#yesButton').replaceWith("<div id=\"yesButton\"><a href=\"#\" data-role=\"button\" data-icon=\"check\" onclick=\"dialogOutput('arkipyh&auml;',8);return true\">Kyll&auml;</a></div>");
	}
	else{
		$('#typeOfLeave').replaceWith("<div id=\"typeOfLeave\">vuosilomap&auml;iv&auml;n</div>");
		$('#yesButton').replaceWith("<div id=\"yesButton\"><a href=\"#\" data-role=\"button\" data-icon=\"check\" onclick=\"dialogOutput('vuosiloma',8);return true\">Kyll&auml;</a></div>");
	}
	$('#dayCheck').replaceWith($('#myDate').val());
	$('#secureQuestion').page("destroy").page();
	//$.mobile.changePage("#secureQuestion");
};

function dialogOutput(output, hours){
	$.mobile.showPageLoadingMsg();
	var olio = {};
	olio["type"] = "hourSlip";
	olio["documentMade"] = new XDate();
	olio["username"] = sessionStorage.getItem('username');
	olio["date"] = new XDate(sessionStorage.getItem('unformDate'));
	olio["client"] = "S&auml;hk&ouml;ansio";
	olio["addWork"] = false;
	olio["addWorkData"] = "";
	olio["workData"] = "";
	olio["basicHourType"] = "";
	olio["overTimeData"] = "";
	olio["overHourType"] = "";
	olio["meal"] = false;
	olio["perDiem"] = "";
	olio["usedCar"] = "";
	olio["km"] = 0;
	olio["otherTextField"] = "";
	olio["carTextField"] = "";
	olio["basicHours"] = 0;
	olio["overTimeHours50"] = 0;
	olio["overTimeHours100"] = 0;
	olio["addSunday"] = 0;
	olio["addEveningShift"] = 0;
	olio["addNightShift"] = 0;
	olio["addEvening"] = 0;
	olio["addNight"] = 0;
	olio["tripHours"] = 0;
	if (output == "vuosiloma"){
		olio["KTA2Hours"] = parseInt(hours);
		olio["KTA1Hours"] = 0;
	}
	else{
		olio["KTA1Hours"] = parseInt(hours);
		olio["KTA2Hours"] = 0;
	}
	olio["KTAData"] = output;
	olio["specDirtyHours"] = 0;
	olio["specHardHours"] = 0;
	olio["specCarHours"] = 0;
	olio["specSuperHours"] = 0;
	olio["menAtWork"] = 0;
	olio["headManHours"] = 0;

	// Send the post to the server and disable post button for the time of the request
	// if the request results in an error, enable the button again. No default error handling for now.
	//$('#sendFormButton').attr("disabled","disabled"); 
	//$('#sendFormButton').button('refresh');
	$.ajax({
		type: 'POST',
		dataType: 'json',
		contentType: "application/json",
		data: JSON.stringify(olio),
		url: "http://54.228.200.239/sansio/",
		success: function(paluuarvo){
			console.log("success:paluuarvo:");
			console.log(paluuarvo);
			//$.mobile.changePage("#day")
			window.location.href = "/sansio/sivut/valikko.html";
			$.mobile.hidePageLoadingMsg();
		},
		error: function(paluuarvo){
			console.log("error:paluuarvo:");
			$('#sendFormButton').button('enable'); 
			$('#sendFormButton').button('refresh');
			$.mobile.hidePageLoadingMsg();
		}
	});
};

function getClients(){
	$.mobile.showPageLoadingMsg();
	$.ajax({
        dataType: 'json',
 		contentType: "application/json",
 		url: "http://54.228.200.239/sansio/_design/views/_view/clientsByName",
 		success: function(sdata){
			console.log("data received");
     		console.log(sdata);
			injectClients(sdata);
        }
    });
};

function saveClient(){
	/*
	OLION SIS�LT�:
	clientName	$('#otherClient').val()
	*/
	var olio = {};
	olio["name"] = $('#otherClient').val();
	olio["adress"] = "";
	console.log("Nimi: " + $('#otherClient').val());

	olio["type"] = "client";
	console.log("dokumentin tyypiksi asetettu client");
	
	$.ajax({
		type: 'POST',
		dataType: 'json',
		contentType: "application/json",
		data: JSON.stringify(olio),
		url: "http://54.228.200.239/sansio/",
		success: function(paluuarvo){
			console.log("paluuarvo:");
			console.log(paluuarvo);
		}
	});
};

function injectClients(clients){
	var max = clients["rows"].length;
	var HTMLbegin = "<select name=\"savedClients\" id=\"savedClients\" data-native-menu=\"false\">";
	var HTML = "";
	var HTMLend = "</select>";
	
	for (var i = 0 ; i < max ; i++ ){
		HTML = HTML + "<option value=\"" + clients["rows"][i]["value"].name + "\">" + clients["rows"][i]["value"].name + "</option>";
	}
	
	HTML = HTMLbegin + HTML + HTMLend;
	
	$('#listOfClients').replaceWith(HTML);
	$.mobile.hidePageLoadingMsg();
	//$('#savedClients').page("destroy").page();

};

function getRoute(){
	if ($('#otherClient').val()){
		var adress = "OSOITE";
		injectRoute(adress);
	}
	else{
		var client = $('#savedClients').val();
		$.ajax({
			dataType: 'json',
			contentType: "application/json",
			url: "http://54.228.200.239/sansio/_design/views/_view/clientsByName",
			success: function(clients){
				console.log("data received");
				var max = clients["rows"].length;
				var adress = "";
				for (var i = 0 ; i < max ; i++ ){
					if (clients["rows"][i]["value"].name == client){
						adress = clients["rows"][i]["value"].adress;
					}
				}
				injectRoute(adress);
			}
		});
	}
};

function injectRoute(adress){
	var home = "Hatanp&auml;&auml;n valtatie 34";
	var HTMLbegin = "<div id=\"carRouteDiv\"><h4>Reitti:</h4><textarea data-theme=\"a\" name=\"carTextField\" id=\"carTextField\" value=\"\">" + home +" - ";
	var HTML = adress;
	var HTMLend = " - " + home + "</textarea></div>";
	
	HTML = HTMLbegin + HTML + HTMLend;
	
	$('#carRouteDiv').replaceWith(HTML);

};

function showSavedHoursForClients(){
	$.mobile.showPageLoadingMsg();
	$.ajax({
        dataType: 'json',
 		contentType: "application/json",
 		url: "http://54.228.200.239/sansio/_design/views/_view/hourSlipsByUser?key=\"" + sessionStorage.getItem('username') + "\"",
 		success: function(sdata){
			console.log("data received");
     		//console.log(sdata);
			getSavedHoursForClients(sdata);
        }
    });
};

function getSavedHoursForClients(users){
	var max = users["rows"].length;
	var hours = 0;
	var temp = new XDate(sessionStorage.getItem('unformDate'));
	var HTMLBegin = "<div id=\"clientHours\"><ul data-role=\"listview\">";
	var HTML = "";
	var HTMLEnd = "</ul></div>";
	
	for (var i = 0 ; i < max ; i++)
	{
		if (users["rows"][i]["value"].date == temp){
			hours = users["rows"][i]["value"].basicHours + users["rows"][i]["value"].KTA1Hours + users["rows"][i]["value"].KTA2Hours;
			HTML = HTML + "<li><a href=\"\" id=\"users\['rows'\][" + i + "]\" onclick=\"checkForm(" + i + ");changePage('#check');return true;\">" + users["rows"][i]["value"].client + "<span class=\"ui-li-count\">" + hours + "</span></a></li>";
		}
	}	
	$('#clientHours').replaceWith(HTMLBegin + HTML + HTMLEnd);
	$('#client').page("destroy").page();
	$.mobile.hidePageLoadingMsg();
};

function changePage(nextID){
	console.log("Changing page to: " + nextID);
	if (FormChanged){
		finalForm();
		$.mobile.changePage("#final");
		FormChanged = false;
		//$('#final').page("destroy").page();
	}
	else{
		$.mobile.changePage(nextID);
	}
};

function finalForm(){
	$.mobile.showPageLoadingMsg();
	console.log("Filling final form");
	$('#finalDay').replaceWith(sessionStorage.getItem('date') + " vko " + sessionStorage.getItem('week'));
	if ($("#addWorkCheckBox").prop("checked")){
		if ($('#otherClient').val()){
			$('#finalClient').replaceWith("<div id=\"finalClient\">" + $('#otherClient').val() + " / " + $('#addWorkText').val() + "</div>");
		}
		else{
			$('#finalClient').replaceWith("<div id=\"finalClient\">" + $('#savedClients').val() + " / " + $('#addWorkText').val() + "</div>");
		}
	}
	else{
		if ($('#otherClient').val()){
			$('#finalClient').replaceWith("<div id=\"finalClient\">" + $('#otherClient').val() + "</div>");
		}
		else{
			$('#finalClient').replaceWith("<div id=\"finalClient\">" + $('#savedClients').val() + "</div>");
		}
	}
	if ($('#otherWorkData').val()){
		$('#finalWorkData').replaceWith("<div id=\"finalWorkData\">" + $('#otherWorkData').val() + "</div>");
	}
	else{
		$('#finalWorkData').replaceWith("<div id=\"finalWorkData\">" + $('#workData').val() + "</div>");
	}
	$('#finalHours').replaceWith("<div id=\"finalHours\">" + $('#basicHoursField').val() + "</div>");
	if($("#mealCheckBox").prop("checked")){
		$('#finalMeal').replaceWith("<div id=\"finalMeal\">, Ateriakorvaus</div>");
	}
	else{
		$('#finalMeal').replaceWith("<div id=\"finalMeal\"></div>");
	}
	$('#finalHourType').replaceWith("<div id=\"finalHourType\">" + $('input:radio[name=hourTypeButton]:checked').val() + " tuntia</div>");
	if ($("#specialBonusesCheckBox").prop("checked")){
		$('#specialBonusesDescription').show();
		var bonuses = "";
		if ($("#special1CheckBox").prop("checked")){
			bonuses = bonuses + "<font size=\"4\">Eritt&auml;in likaisen ja eritt&auml;in raskaan ty&ouml;n lis&auml;: </font>" + $('#special1Hours').val() + " tuntia <br>";
		}
		if ($("#special2CheckBox").prop("checked")){
			bonuses = bonuses + "<font size=\"4\">Vaativan ja hankalan ty&ouml;n lis&auml;: </font>" + $('#special2Hours').val() + " tuntia <br>";
		}
		if ($("#special3CheckBox").prop("checked")){
			bonuses = bonuses + "<font size=\"4\">Autonkuljettajalis&auml;: </font>" + $('#special3Hours').val() + " tuntia <br>";
		}
		if ($("#special4CheckBox").prop("checked")){
			bonuses = bonuses + "<font size=\"4\">Ty&ouml;paikkaohjaajalis&auml;: </font>" + $('#special4Hours').val() + " tuntia <br>";
		}
		$('#finalSpecialBonuses').replaceWith("<div id=\"finalSpecialBonuses\">" + bonuses + "</div>");
	}
	else{
		$('#specialBonusesDescription').hide();
	}
	if (($('#overTimeHours50').val() != 0) || ($('#overTimeHours100').val() != 0)){
		$('#OverTimeDescription').show();
		if ($('#otherOverTimeData').val()){
			$('#finalOverTimeData').replaceWith("<div id=\"finalOverTimeData\">" + $('#otherOverTimeData').val() + "</div>");
		}
		else{
			$('#finalOverTimeData').replaceWith("<div id=\"finalOverTimeData\">" + $('#overTimeData').val() + "</div>");
		}
		$('#finalOverHours').replaceWith("<div id=\"finalOverHours\">50%: " + $('#overTimeHours50').val() + ", 100%: " + $('#overTimeHours100').val() + " tuntia</div>");
	}
	else{
		$('#OverTimeDescription').hide();
		$('#finalOverHours').replaceWith("<div id=\"finalOverHours\">Ei</div>");
		$('#finalOverHourType').replaceWith("<div id=\"finalOverHourType\"></div>");
	}
	if ($("#moreOverWorkCheckBox").prop("checked")){
		$('#moreOverTimeDescription').show();
		var moreOverWork = "";
		if ($('#moreOverWorkHours1').val() != 0){
			moreOverWork = moreOverWork + "<font size=\"4\">Sunnuntailis&auml;: </font>" + $('#moreOverWorkHours1').val() + " tuntia <br>";
		}
		if ($('#moreOverWorkHours2').val() != 0){
			moreOverWork = moreOverWork + "<font size=\"4\">Iltaty&ouml;lis&auml;: </font>" + $('#moreOverWorkHours4').val() + " tuntia <br>";
		}
		if ($('#moreOverWorkHours3').val() != 0){
			moreOverWork = moreOverWork + "<font size=\"4\">Y&ouml;ty&ouml;lis&auml;: </font>" + $('#moreOverWorkHours5').val() + " tuntia <br>";
		}
		$('#finalMoreOverTimeData').replaceWith("<div id=\"finalMoreOverTimeData\">" + moreOverWork + "</div>");
	}
	else{
		$('#moreOverTimeDescription').hide();
	}
	if ($("#headManCheckBox").prop("checked")){
		$('#finalHeadMan').replaceWith("<div id=\"finalHeadMan\">Kyll&auml;, </div>");
		$('#finalMen').replaceWith("<div id=\"finalMen\">" + $('#menAtWork').val() + " miest&auml; ty&ouml;maalla</div>");
		$('#finalHeadHours').replaceWith("<div id=\"finalHeadHours\">, " + $('#headManHours').val() + " tuntia</div>");
		}
	else{
		$('#finalHeadMan').replaceWith("<div id=\"finalHeadMan\">Ei</div>");
		$('#finalMen').replaceWith("<div id=\"finalMen\"></div>");
		$('#finalHeadHours').replaceWith("<div id=\"finalHeadHours\"></div>");
	}
	if ($("#perDiemRadioNo").prop("checked")){
		$('#finalPerDiem').replaceWith("<div id=\"finalPerDiem\">Ei</div>");
		$('#finalTripHours').replaceWith("<div id=\"finalTripHours\"></div>");
	}
	else{
		$('#finalPerDiem').replaceWith("<div id=\"finalPerDiem\">" + $('input:radio[name=perDiemRadio]:checked').val() + "</div>");
		$('#finalTripHours').replaceWith("<div id=\"finalTripHours\">, " + $('#tripHours').val() + " matkatuntia</div>");
	}
	if ($('#km').val() != 0){
		$('#finalUsedCar').replaceWith("<div id=\"finalUsedCar\">" + $('input:radio[name=usedCarButton]:checked').val() + ": </div>");
		$('#finalKm').replaceWith("<div id=\"finalKm\">" + $('#km').val() + " kilometri&auml;</div>");
		$('#finalRoute').replaceWith("<div id=\"finalRoute\">" + $('textarea#carTextField').val() + "</div>");
	}
	else{
		$('#finalUsedCar').replaceWith("<div id=\"finalUsedCar\">Ei kilometrej&auml;</div>");
		$('#finalKm').replaceWith("<div id=\"finalKm\"></div>");
		$('#finalRoute').replaceWith("<div id=\"finalRoute\"></div>");
	}
	$('#finalOther').replaceWith("<div id=\"finalOther\">" + $('textarea#otherTextField').val() + "</div>");
	//$('#final').page("destroy").page();
	$.mobile.hidePageLoadingMsg();
};

function checkForm(row){
	console.log(row);
	$.ajax({
        dataType: 'json',
 		contentType: "application/json",
 		url: "http://54.228.200.239/sansio/_design/views/_view/hourSlipsByUser?key=\"" + sessionStorage.getItem('username') + "\"",
 		success: function(users){
			console.log("data received");
			console.log("Checking form");
			console.log("HourSlip id: " + users["rows"][row]["value"]._id);
			console.log("HourSlip rev: " + users["rows"][row]["value"]._rev);
			$('#checkDay').replaceWith(users["rows"][row]["value"].day);
			if (users["rows"][row]["value"].addWork){
				$('#checkClient').replaceWith("<div id=\"checkClient\">" + users["rows"][row]["value"].client + " / " + users["rows"][row]["value"].addWorkData + "</div>");
			}
			else{
				$('#checkClient').replaceWith("<div id=\"checkClient\">" + users["rows"][row]["value"].client + "</div>");
			}
			if (users["rows"][row]["value"].workData){
				$('#checkWorkData').replaceWith("<div id=\"checkWorkData\">" + users["rows"][row]["value"].workData + "</div>");
			}
			else{
				$('#checkWorkData').replaceWith("<div id=\"checkWorkData\">" + users["rows"][row]["value"].KTAData + "</div>");
			}
			if (users["rows"][row]["value"].KTA1Hours != 0){
				$('#checkHours').replaceWith("<div id=\"checkHours\">" + users["rows"][row]["value"].KTA1Hours + "</div>");
			}
			else if (users["rows"][row]["value"].KTA2Hours != 0){
				$('#checkHours').replaceWith("<div id=\"checkHours\">" + users["rows"][row]["value"].KTA2Hours + "</div>");
			}
			else{
				$('#checkHours').replaceWith("<div id=\"checkHours\">" + users["rows"][row]["value"].basicHours + "</div>");
			}
			if (users["rows"][row]["value"].meal){
				$('#checkMeal').replaceWith("<div id=\"checkMeal\">, Ateriakorvaus</div>");
			}
			else{
				$('#checkMeal').replaceWith("<div id=\"checkMeal\"></div>");
			}
			$('#checkHourType').replaceWith("<div id=\"checkHourType\">" + users["rows"][row]["value"].basicHourType + " tuntia</div>");
			if (users["rows"][row]["value"].specDirtyHours != 0 || users["rows"][row]["value"].specHardHours != 0 || users["rows"][row]["value"].specCarHours != 0 || users["rows"][row]["value"].specSuperHours != 0){
				$('#checkSpecialAdds').show();
				if (users["rows"][row]["value"].specDirtyHours != 0){
					$('#checkSpec1').replaceWith("<div id=\"checkSpec1\">" + users["rows"][row]["value"].specDirtyHours + "</div>");
				}
				else{
					$('#checkSpec1').replaceWith("<div id=\"checkSpec1\"> Ei</div>");
				}
				if (users["rows"][row]["value"].specHardHours != 0){
					$('#checkSpec2').replaceWith("<div id=\"checkSpec2\">" + users["rows"][row]["value"].specHardHours + "</div>");
				}
				else{
					$('#checkSpec2').replaceWith("<div id=\"checkSpec2\"> Ei</div>");
				}
				if (users["rows"][row]["value"].specCarHours != 0){
					$('#checkSpec3').replaceWith("<div id=\"checkSpec3\">" + users["rows"][row]["value"].specCarHours + "</div>");
				}
				else{
					$('#checkSpec3').replaceWith("<div id=\"checkSpec3\"> Ei</div>");
				}
				if (users["rows"][row]["value"].specSuperHours != 0){
					$('#checkSpec4').replaceWith("<div id=\"checkSpec4\">" + users["rows"][row]["value"].specSuperHours + "</div>");
				}
				else{
					$('#checkSpec4').replaceWith("<div id=\"checkSpec4\"> Ei</div>");
				}
			}
			else{
				$('#checkSpecialAdds').hide();
			}
			if ((users["rows"][row]["value"].overTimeHours50 != 0) || (users["rows"][row]["value"].overTimeHours100 != 0)){
				$('#checkOverTimeData').replaceWith("<div id=\"checkOverTimeData\">" + users["rows"][row]["value"].overTimeData + "</div>");
				$('#checkOverHours').replaceWith("<div id=\"checkOverHours\"> 50%: " + users["rows"][row]["value"].overTimeHours50 + ", 100%: " + users["rows"][row]["value"].overTimeHours100 + " tuntia</div>");
			}
			else{
				$('#checkOverTimeData').replaceWith("<div id=\"checkOverTimeData\">Ei</div>");
				$('#checkOverHours').replaceWith("<div id=\"checkOverHours\">Ei</div>");
				$('#checkOverHourType').replaceWith("<div id=\"checkOverHourType\"></div>");
			}
			if ( (users["rows"][row]["value"].addSunday != 0) || (users["rows"][row]["value"].addEvening != 0) || (users["rows"][row]["value"].addNight != 0)){
				$('#checkOverAdds').show();
				if (users["rows"][row]["value"].addSunday != 0){
					$('#checkMoreOverWork1').replaceWith("<div id=\"checkMoreOverWork1\">" + users["rows"][row]["value"].addSunday + "</div>");
				}
				else{
					$('#checkMoreOverWork1').replaceWith("<div id=\"checkMoreOverWork1\">Ei</div>");
				}
				if (users["rows"][row]["value"].addEvening != 0){
					$('#checkMoreOverWork2').replaceWith("<div id=\"checkMoreOverWork2\">" + users["rows"][row]["value"].addEvening + "</div>");
				}
				else{
					$('#checkMoreOverWork2').replaceWith("<div id=\"checkMoreOverWork2\">Ei</div>");
				}
				if (users["rows"][row]["value"].addNight != 0){
					$('#checkMoreOverWork3').replaceWith("<div id=\"checkMoreOverWork3\">" + users["rows"][row]["value"].addNight + "</div>");
				}
				else{
					$('#checkMoreOverWork3').replaceWith("<div id=\"checkMoreOverWork3\">Ei</div>");
				}
			}
			else{
				$('#checkOverAdds').hide();
			}
			if (users["rows"][row]["value"].headMan){
				$('#checkHeadMan').replaceWith("<div id=\"checkHeadMan\">Kyll&auml;, </div>");
				$('#checkMen').replaceWith("<div id=\"checkMen\">" + users["rows"][row]["value"].menAtWork + " miest&auml; ty&ouml;maalla</div>");
				$('#checkHeadHours').replaceWith("<div id=\"checkHeadHours\">, " + users["rows"][row]["value"].headManHours + " tuntia</div>");
			}
			else{
				$('#checkHeadMan').replaceWith("<div id=\"checkHeadMan\">Ei</div>");
				$('#checkMen').replaceWith("<div id=\"checkMen\"></div>");
				$('#checkHeadHours').replaceWith("<div id=\"checkHeadHours\"></div>");
			}
			if (users["rows"][row]["value"].perDiem){
				$('#checkPerDiem').replaceWith("<div id=\"checkPerDiem\">" + users["rows"][row]["value"].perDiem + "</div>");
				$('#checkTripHours').replaceWith("<div id=\"checkTripHours\">, " + users["rows"][row]["value"].tripHours + " matkatuntia</div>");
			}
			else{
				$('#checkPerDiem').replaceWith("<div id=\"checkPerDiem\">Ei</div>");
				$('#checkTripHours').replaceWith("<div id=\"checkTripHours\"></div>");
			}
			if (users["rows"][row]["value"].usedCar){
				$('#checkUsedCar').replaceWith("<div id=\"checkUsedCar\">" + users["rows"][row]["value"].usedCar + ": </div>");
			}
			else{
				$('#checkUsedCar').replaceWith("<div id=\"checkUsedCar\">Auto: </div>");
			}
			$('#checkKm').replaceWith("<div id=\"checkKm\">" + users["rows"][row]["value"].km + " kilometri&auml;</div>");
			$('#checkOther').replaceWith("<div id=\"checkOther\">" + users["rows"][row]["value"].otherTextField + "</div>");
			$('#checkRoute').replaceWith("<div id=\"checkRoute\">" + users["rows"][row]["value"].carTextField + "</div>");
			
			$('#deleteButton').replaceWith("<div id=\"deleteButton\">" + "<a href=\"\" data-role=\"button\" onclick=\"deleteHourSlip('" + users["rows"][row]["value"]._id + "','" + users["rows"][row]["value"]._rev + "');return false;\">Poista</a>" + "</div>");
			$('#check').page("destroy").page();
        }
    });
};

function changeForm(){
	FormChanged = true;
	console.log("FormChanged: " + FormChanged);
};

function sendForm(){
	$.mobile.showPageLoadingMsg();
	/* Tallennetaan uusi tilaaja, jos l�ytyy */
	if ($('#otherClient').val()){
		saveClient();
	}
	console.log("Begin to send form.");
	/*
	FORMIN SIS�LT�:
	
	P�iv�kohtaiset:	
	type 			"hourSlip";
	documentMade	xdate.toString();
	username		sessionStorage.getItem('username')
	week			sessionStorage.getItem('week')
	date			sessionStorage.getItem('unformDate');
	client			$('#savedClients').val() OR $('#otherClient').val()
	clientNumber	getClientNumber()
	addWork			$("#addWorkCheckBox").prop("checked")
	addWorkData		$('#addWorkText').val()
	KTAData			""
	workData		$('#workData').val() OR $('#otherWorkData').val()
	basicHourType	$('input:radio[name=hourTypeButton]:checked').val()
	overTimeData	$('#overTimeData').val() OR $('#otherOverTimeData').val()
	meal			$("#mealCheckBox").prop("checked")
	perDiem			$('input:radio[name=perDiemRadio]:checked').val()
	usedCar			$('input:radio[name=usedCarButton]:checked').val()
	km				$('#km').val()
	otherTextField	$('textarea#otherTextField').val()
	carTextField	$('textarea#carTextField').val()
	
	P�iv�n erilaiset tunnit:
	basicHours		$('#basicHoursField').val()
	overTimeHours50	$('#overTimeHours50').val()
	overTimeHours100$('#overTimeHours100').val()
	addSunday		$('#moreOverWorkHours1').val()
	addEvening		$('#moreOverWorkHours2').val()
	addNight		$('#moreOverWorkHours3').val()
	tripHours		$('#tripHours').val()
	KTA1Hours		0
	KTA2Hours		0
	
	Tunneille maksettavat lis�t:
	specDirtyHours	$('#special1Hours').val()
	specHardHours	$('#special2Hours').val()
	specCarHours	$('#special3Hours').val()
	specSuperHours	$('#special4Hours').val()
	menAtWork		$('#menAtWork').val()
	headManHours	$('#headManHours').val()
	*/
	var olio = {};
	var dateInMilli = new XDate(sessionStorage.getItem('unformDate'));
	var documentMadeInMilli = new XDate();
	olio["type"] = "hourSlip";
	olio["documentMade"] = documentMadeInMilli.getTime();
	olio["username"] = sessionStorage.getItem('username');
	olio["date"] = dateInMilli.getTime();
	if ($('#otherClient').val()){
		olio["client"] = $('#otherClient').val();
	}
	else{
		olio["client"] = $('#savedClients').val();
	}
	olio["clientNumber"] = sessionStorage.getItem('number');
	olio["addWork"] = $("#addWorkCheckBox").prop("checked");
	olio["addWorkData"] = $('#addWorkText').val();
	olio["KTAData"] = "";
	if ($('#otherWorkData').val()){
		olio["workData"] = $('#otherWorkData').val();
	}
	else{
		olio["workData"] = $('#workData').val();
	}
	olio["basicHourType"] = $('input:radio[name=hourTypeButton]:checked').val();
	if (parseInt($('#overTimeHours50').val()) != 0 || parseInt($('#overTimeHours100').val()) != 0){
		if ($('#otherOverTimeData').val()){
			olio["overTimeData"] = $('#otherOverTimeData').val();
		}
		else{
			olio["overTimeData"] = $('#overTimeData').val();
		}
	}
	else{
		olio["overTimeData"] = "";
	}
	olio["meal"] = $("#mealCheckBox").prop("checked");
	olio["perDiem"] = $('input:radio[name=perDiemRadio]:checked').val();
	olio["usedCar"] = $('input:radio[name=usedCarButton]:checked').val();
	olio["km"] = parseInt($('#km').val());
	olio["otherTextField"] = $('textarea#otherTextField').val();
	olio["carTextField"] = $('textarea#carTextField').val();
	
	olio["basicHours"] = parseInt($('#basicHoursField').val());
	olio["overTimeHours50"] = parseInt($('#overTimeHours50').val());
	olio["overTimeHours100"] = parseInt($('#overTimeHours100').val());
	olio["addSunday"] = parseInt($('#moreOverWorkHours1').val());
	olio["addEvening"] = parseInt($('#moreOverWorkHours2').val());
	olio["addNight"] = parseInt($('#moreOverWorkHours3').val());
	olio["tripHours"] = parseInt($('#tripHours').val());
	olio["KTA1Hours"] = 0;
	olio["KTA2Hours"] = 0;
	
	if ($("#special1CheckBox").prop("checked")){
		olio["specDirtyHours"] = parseInt($('#special1Hours').val());
	}
	else{
		olio["specDirtyHours"] = 0;
	}
	if ($("#special2CheckBox").prop("checked")){
		olio["specHardHours"] = parseInt($('#special2Hours').val());
	}
	else{
		olio["specHardHours"] = 0;
	}
	if ($("#special3CheckBox").prop("checked")){
		olio["specCarHours"] = parseInt($('#special3Hours').val());
	}
	else{
		olio["specCarHours"] = 0;
	}
	if ($("#special4CheckBox").prop("checked")){
		olio["specSuperHours"] = parseInt($('#special4Hours').val());
	}
	else{
		olio["specSuperHours"] = 0;
	}
	if ($("#headManCheckBox").prop("checked")){
		olio["menAtWork"] = $('#menAtWork').val();
		olio["headManHours"] = parseInt($('#headManHours').val());
	}
	else{
		olio["menAtWork"] = "";
		olio["headManHours"] = 0;
	}
	// Send the post to the server and disable post button for the time of the request
	// if the request results in an error, enable the button again. No default error handling for now.
	$('#sendFormButton').attr("disabled","disabled"); 
	$('#sendFormButton').button('refresh');
	$.ajax({
		type: 'POST',
		dataType: 'json',
		contentType: "application/json",
		data: JSON.stringify(olio),
		url: "http://54.228.200.239/sansio/",
		success: function(paluuarvo){
			console.log("success:paluuarvo:");
			console.log(paluuarvo);
			//$.mobile.changePage("#day")
			window.location.href = "/sansio/sivut/valikko.html";
			$.mobile.hidePageLoadingMsg();
		},
		error: function(paluuarvo){
			console.log("error:paluuarvo:");
			$('#sendFormButton').button('enable'); 
			$('#sendFormButton').button('refresh');
			$.mobile.hidePageLoadingMsg();
		}
	});
};

function getClientNumber(){
	var number = 0;
	$.ajax({
        dataType: 'json',
 		contentType: "application/json",
 		url: "http://54.228.200.239/sansio/_design/views/_view/clientsByName",
 		success: function(clients){
			var max = clients["rows"].length;
			for (var i = 0 ; i < max ; i++){
				console.log($('#savedClients').val() + " vs. " + clients["rows"][i]["value"].name + clients["rows"][i]["value"].number);
				if ($('#savedClients').val() == clients["rows"][i]["value"].name){
					sessionStorage.setItem('number', clients["rows"][i]["value"].number);
				}
			}
        }
    });
};

function deleteHourSlip(id,rev){
	console.log("Deleting URL: http://54.228.200.239/sansio/" + id + "?rev=" + rev);
	$.ajax({
         type: 'DELETE',
         url: "http://54.228.200.239/sansio/" + id + "?rev=" + rev,
         success: function () {
            console.log("HourSlip deleted!");
			$.mobile.changePage("#client");
			showSavedHoursForClients($('#myDate').val());
			$('#client').page("destroy").page();
         }
    });
};

function specialBonuses(){
	if ($("#specialBonusesCheckBox").prop("checked")){
		$.mobile.changePage("#specialBonusesPage");
	}
};

function moreOverWork(){
	if ($("#moreOverWorkCheckBox").prop("checked")){
		$('#moreOverWorkFields').show();
	}
	else{
		$('#moreOverWorkFields').hide();
	}
};