$(document).ready(function() {
	// console.log("document ready");
	get_wagePeriods();
	checkLogin();
	// showUser();

});

function getHourSlips(firstWeek, lastWeek){

	//Haetaan:
	// tuntilaput, joiden tiedoissa on viikkona "firstWeek" tai "lastWeek" ja k�ytt�j�n� user.
	
	var ekaViikko = parseInt(firstWeek);
	// console.log("Eka viikko: " + ekaViikko );
	var tokaViikko = parseInt(lastWeek);
	// console.log("Toka viikko: " + tokaViikko );
	var URLBegin = "http://54.228.200.239/sansio/_design/views/_view/hourSlipsByDateAndUser?startkey=[\"";
	var URLMiddle = "\",";
	var URLMiddle2 = "]&endkey=[\"";
	var URLEnd = "]";
	var user = sessionStorage.getItem('username');
	
	$.ajax({
        dataType: 'json',
 		contentType: "application/json",
 		url: URLBegin + user + URLMiddle + ekaViikko + URLMiddle2 + user + URLMiddle + tokaViikko + URLEnd,
 		success: function(sdata){
			// console.log("slip data received");
     		// console.log(sdata);
			injectData(sdata, firstWeek, lastWeek);
        }
    });
}

function injectData(hourSlips, firstWeek, lastWeek){
	
	// Palkkajakson alkua ja loppu rakennetaan xdate-objekteiksi, jotta niit� voidaan k�sitell� helpommin.
	var periodStart = new XDate( firstWeek );
	// console.log("Palkkajakson alku: " + periodStart.toString() );
	var periodEnd = new XDate( lastWeek );
	// console.log("Palkkajakson loppu: " + periodEnd.toString() );
	
	//T�h�n tallennetaan ne tilaajat, joille k�ytt�j� on tehnyt t�it� palkkajakson aikana.
	var clients = new Array();
	
	// Viikonp�ivien nimet
	var weekday = new Array( 7 );
	weekday[ 0 ] = "Su";
	weekday[ 1 ] = "Ma";
	weekday[ 2 ] = "Ti";
	weekday[ 3 ] = "Ke";
	weekday[ 4 ] = "To";
	weekday[ 5 ] = "Pe";
	weekday[ 6 ] = "La";
	
	// Koko palkkajaksoa koskevat yhteism��r�t.
	var allHours = 0;
	var allOverhours = 0;
	var allHeadmans = 0;
	var allMyCars = 0;
	var allCompCars = 0;
	var allMeals = 0;
	
	var HTML ="\
	<div id=\"slips\">\
			<p>Tunnit yhteens&auml;:</p>\
			<table data-role=\"table\" data-column-btn-text=\"N&auml;yt&auml;\" data-mode=\"columntoggle\" id=\"combined-data\" class=\"ui-responsive table-stroke\">\
				<thead>\
					<tr>\
						<th data-priority=\"\">P&auml;iv&auml;</th>\
						<th data-priority=\"\">pv</th>\
						<th data-priority=\"\">kk</th>\
						<th data-priority=\"4\">vv</th>\
						<th data-priority=\"5\">Tunnit</th>\
						<th data-priority=\"6\">Ylity&ouml;tunnit</th>\
						<th data-priority=\"6\">K&auml;rkimies tunnit</th>\
						<th data-priority=\"6\">Oman auton km:t</th>\
						<th data-priority=\"6\">Firman auton km:t</th>\
						<th data-priority=\"6\">Ateriakorvaukset</th>\
						<!--\
						<th data-priority=\"1\"><br>P&auml;iv&auml;</th>\
						<th data-priority=\"2\"><br>pv</th>\
						<th data-priority=\"3\"><br>kk</th>\
						<th data-priority=\"4\"><br>vv</th>\
						<th data-priority=\"4\">Ty&ouml;-<br>p&auml;iv&auml;t</th>\
						<th data-priority=\"5\"><br>ATP</th>\
						<th data-priority=\"5\"><br>UTP</th>\
						<th data-priority=\"5\"><br>UKP</th>\
						<th data-priority=\"5\"><br>KTA</th>\
						<th data-priority=\"6\">Km-<br>korvaus</th>\
						<th data-priority=\"6\">Ateria-<br>korvaus</th>\
						<th data-priority=\"6\">K&auml;rkimies<br>1-2</th>\
						<th data-priority=\"6\">K&auml;rkimies<br>3-6</th>\
						<th data-priority=\"6\">K&auml;rkimies<br>7-10</th>\
						<th data-priority=\"6\">Ylity&ouml;<br>vrk 50%</th>\
						<th data-priority=\"6\">Ylity&ouml;<br>vrk 100%</th>-->\
					</tr>\
				</thead>\
				<tbody>";
				
	// K�yd��n palkkajakso l�pi alkaen jakson alusta ja p��tet��n jakson loppuun. 
	var askWP = 1;
	for( var dayWalker = new XDate( periodStart ); dayWalker < periodEnd; dayWalker.addDays( 1 ) )
	{
		var wkdaystring = "wkday" + askWP.toString();
		var daystring = "day" + askWP.toString();
		var monthstring = "month" + askWP.toString();
		var yearstring = "year" + askWP.toString();
		var hoursstring = "hours" + askWP.toString();
		// var datestring = "date" + askWP.toString();
		var overhoursstring = "overhours" + askWP.toString();
		var headmanstring = "headman" + askWP.toString();
		var myCarstring = "myCar" + askWP.toString();
		var compCarstring = "compCar" + askWP.toString();
		var mealstring = "meal" + askWP.toString();
		
		// Haetaan p�iv�m��r� ja jaotellaan siit� viikonp�iv�, pp,kk ja vv.
		var wkday = weekday[ dayWalker.getDay() ];
		var day = dayWalker.getDate();
		var month = dayWalker.getMonth() + 1; // Kuukaudet numeroina indeksoiden nollasta.
		var year = dayWalker.getFullYear();
		
		// Yhden p�iv�n sis��n laskettavat muuttujat.
		var hours = 0;
		var overhours = 0;
		var headmans = 0;
		var myCars = 0;
		var compCars = 0;
		var meals = 0;
		
		// Selvitet��n p�iv�m��r�� vastaavat tehdyt ty�tunnit:
		// K�yd��n l�pi hourSlips ja haetaan niist� ne, joiden p�iv�m��r� on sama kuin nyt k�sittelyss� oleva eli dayWalker.
		for (var askHS = 0; askHS < hourSlips["rows"].length; askHS++)
		{
		
			// console.log("wagepvm1: " + dayWalker.toString( ) );
			// console.log("slippvm1: " + hourSlips["rows"][askHS]["value"].date );
			if( dayWalker.getTime( ) == hourSlips["rows"][askHS]["value"].date )
			{
				
				// console.log("Edelliset pvmt olivat samat.");
				// Testataan, onko t�h�n tuntilappuun liitetty tilaaja jo otettu huomioon.
				var clientTestIndex = clients.indexOf( hourSlips["rows"][askHS]["value"].client );
				
				// console.log("Testataan tilaajaa: " + hourSlips["rows"][askHS]["value"].client);
				// console.log("Tilaajan index: " + clientTestIndex );
				
				if( clientTestIndex == -1 )
				{
					
					// console.log("Tilaajaa ei ollut ennen tallennettu.");
					// Jos tilaajaa ei viel� ole lis�tty, lis�t��n se nyt.
					clients[clients.length] = hourSlips["rows"][askHS]["value"].client;
				}
				
				else
				{
					
					// console.log("Tilaaja oli jo tallennettuna.");
				}
				
				// Lis�t��n t�h�n tuntilappuun liittyv�t muuttujat mukaan.
				
				// console.log("Lis�t��n tavallisia tunteja: " + hourSlips["rows"][askHS]["value"].basicHours );
				
				hours = hours + hourSlips["rows"][askHS]["value"].basicHours;
				
				// console.log("Nyt p�iv�m��r�ll� " + dayWalker.toString() + " on yhteens� " + hours + " tuntia.");
				
				overhours = overhours + hourSlips["rows"][askHS]["value"].overTimeHours100 + hourSlips["rows"][askHS]["value"].overTimeHours50;
				headmans = headmans + hourSlips["rows"][askHS]["value"].headManHours;
				
				if( hourSlips["rows"][askHS]["value"].usedCar == "Oma auto" )
				{
					myCars = myCars + hourSlips["rows"][askHS]["value"].km;
				}
				if( hourSlips["rows"][askHS]["value"].usedCar == "Firman auto" )
				{
					compCars = compCars + hourSlips["rows"][askHS]["value"].km;
				}
				if( hourSlips["rows"][askHS]["value"].meal )
				{
					meals++;
				}
			}
		}
		
		// Lis�t��n yksitt�isen p�iv�n saldot kokonaissummiin.
		allHours = allHours + hours;
		allOverhours = allOverhours + overhours;
		allHeadmans = allHeadmans + headmans;
		allMyCars = allMyCars + myCars;
		allCompCars = allCompCars + compCars;
		allMeals = allMeals + meals;
		
		// Korvataan yksitt�isen p�iv�n tiedot dokkariin.
		HTML = HTML + "\
		<tr>\
			<td id=\"" + wkdaystring + "\">" + wkday + "</td>\
			<td id=\"" + daystring + "\">" + day + "</td>\
			<td id=\"" + monthstring + "\">" + month + "</td>\
			<td id=\"" + yearstring + "\">" + year + "</td>\
			<td id=\"" + hoursstring + "\">" + hours + "</td>\
			<td id=\"" + overhoursstring + "\">" + overhours + "</td>\
			<td id=\"" + headmanstring + "\">" + headmans + "</td>\
			<td id=\"" + myCarstring + "\">" + myCars + "</td>\
			<td id=\"" + compCarstring + "\">" + compCars + "</td>\
			<td id=\"" + mealstring + "\">" + meals + "</td>\
		</tr>";
		
		askWP++;
	}
	
	// Lis�t��n yhteenlaskujen tulokset summasarakkeeseen.
	HTML = HTML + "<tr>\
		<td>-</td>\
		<td>-</td>\
		<td>-</td>\
		<td>Yht</td>\
		<td id=\"allHours\">" + allHours + "</td>\
		<td id=\"allOverhours\">" + allOverhours + "</td>\
		<td id=\"allHeadmans\">" + allHeadmans + "</td>\
		<td id=\"allMyCars\">" + allMyCars + "</td>\
		<td id=\"allCompCars\">" + allCompCars + "</td>\
		<td id=\"allMeals\">" + allMeals + "</td>\
	</tr>";
	
	HTML = HTML + "\
				</tbody>\
			</table>\
		<input value=\"Lataa tiedot\" type=\"button\" onclick=\"$('#combined-data').table2CSV()\">\
		<p>Tai tarkastele ty&ouml;maittain:</p>\
		<div id=\"listOfClients\">";
	
	// Lis�t��n tilaajat sivun lopussa olevaan listaan.
	var HTML2begin = "<a href=\"#hourSlipsByClient\" data-role=\"button\" rel=\"internal\" onclick=\"getHourSlipsC('";
	var HTML2middle = ");return true;\" >";
	var HTML2end = "</a>";

	for (var ask = 0 ; ask < clients.length ; ask++ )
	{
		
		// console.log("Lis�t��n tilaaja: " + clients[ask] );
		HTML = HTML + HTML2begin + clients[ask] + "'," + firstWeek + "," + lastWeek + HTML2middle + clients[ask] + HTML2end;
	}
	
	HTML = HTML + "</div></div>";
	// console.log("testi1");
	$('#slips').replaceWith(HTML);
	// console.log("testi2");
	//$(document).ready(function(){$('#hourSlips').page("destroy").page();});
	$('#hourSlips').page("destroy").page();
};

function getHourSlipsC(client, firstWeek, lastWeek){

	//Haetaan:
	// tuntilaput, joiden tiedoissa on viikkona "firstWeek" tai "lastWeek", k�ytt�j�n� user ja tilaajana client.
	
	var ekaViikko = parseInt(firstWeek);
	// console.log("Eka viikko: " + ekaViikko );
	var tokaViikko = parseInt(lastWeek);
	// console.log("Toka viikko: " + tokaViikko );
	var URLBegin = "http://54.228.200.239/sansio/_design/views/_view/hourSlipsByDateAndUserAndClient?startkey=[\"";
	var URLMiddle = "\",";
	var URLMiddle2 = "]&endkey=[\"";
	var URLEnd = "]";
	var user = sessionStorage.getItem('username');
	
	$.ajax({
        dataType: 'json',
 		contentType: "application/json",
 		url: URLBegin + user + "\",\"" + client + URLMiddle + ekaViikko + URLMiddle2 + user + "\",\"" + client + URLMiddle + tokaViikko + URLEnd,
 		success: function(sdata){
			// console.log("slip data received");
     		// console.log(sdata);
			injectDataC(sdata, firstWeek, lastWeek, client);
        }
    });
}

function injectDataC(hourSlips, firstWeek, lastWeek, client){
	
	// Palkkajakson alku ja loppu rakennetaan xdate-objekteiksi, jotta niit� voidaan k�sitell� helpommin.
	var periodStart = new XDate( firstWeek );
	// console.log("Palkkajakson alku: " + periodStart.toString() );
	var periodEnd = new XDate( lastWeek );
	// console.log("Palkkajakson loppu: " + periodEnd.toString() );
	
	// Viikonp�ivien nimet
	var weekday = new Array( 7 );
	weekday[ 0 ] = "Su";
	weekday[ 1 ] = "Ma";
	weekday[ 2 ] = "Ti";
	weekday[ 3 ] = "Ke";
	weekday[ 4 ] = "To";
	weekday[ 5 ] = "Pe";
	weekday[ 6 ] = "La";
	
	// Koko palkkajaksoa koskevat yhteism��r�t.
	var allHours = 0;
	var allOverhours = 0;
	var allHeadmans = 0;
	var allMyCars = 0;
	var allCompCars = 0;
	var allMeals = 0;
	console.log("Debug1");
	var HTML ="\
	<div id=\"slipsByClient\">\
			<p>Tilaajalle \"" + client + "\" tekem&auml;si tunnit yhteens&auml;:</p>\
			<table data-role=\"table\" data-mode=\"columntoggle\" data-column-btn-text=\"N&auml;yt&auml;\" id=\"combined-dataC\" class=\"ui-responsive table-stroke\">\
				<thead>\
					<tr>\
						<th data-priority=\"\">P&auml;iv&auml;</th>\
						<th data-priority=\"\">pv</th>\
						<th data-priority=\"\">kk</th>\
						<th data-priority=\"4\">vv</th>\
						<th data-priority=\"5\">Tunnit</th>\
						<th data-priority=\"6\">Ylity&ouml;tunnit</th>\
						<th data-priority=\"6\">K&auml;rkimies tunnit</th>\
						<th data-priority=\"6\">Oman auton km:t</th>\
						<th data-priority=\"6\">Firman auton km:t</th>\
						<th data-priority=\"6\">Ateriakorvaukset</th>\
						<!--\
						<th data-priority=\"1\"><br>P&auml;iv&auml;</th>\
						<th data-priority=\"2\"><br>pv</th>\
						<th data-priority=\"3\"><br>kk</th>\
						<th data-priority=\"4\"><br>vv</th>\
						<th data-priority=\"4\">Ty&ouml;-<br>p&auml;iv&auml;t</th>\
						<th data-priority=\"5\"><br>ATP</th>\
						<th data-priority=\"5\"><br>UTP</th>\
						<th data-priority=\"5\"><br>UKP</th>\
						<th data-priority=\"5\"><br>KTA</th>\
						<th data-priority=\"6\">Km-<br>korvaus</th>\
						<th data-priority=\"6\">Ateria-<br>korvaus</th>\
						<th data-priority=\"6\">K&auml;rkimies<br>1-2</th>\
						<th data-priority=\"6\">K&auml;rkimies<br>3-6</th>\
						<th data-priority=\"6\">K&auml;rkimies<br>7-10</th>\
						<th data-priority=\"6\">Ylity&ouml;<br>vrk 50%</th>\
						<th data-priority=\"6\">Ylity&ouml;<br>vrk 100%</th>-->\
					</tr>\
				</thead>\
				<tbody>";
				
	// K�yd��n palkkajakso l�pi alkaen jakson alusta ja p��tet��n jakson loppuun. 
	var askWP = 1;
	for( var dayWalker = new XDate( periodStart ); dayWalker < periodEnd; dayWalker.addDays( 1 ) )
	{
		var wkdaystring = "wkday" + askWP.toString() + "C";
		var daystring = "day" + askWP.toString() + "C";
		var monthstring = "month" + askWP.toString() + "C";
		var yearstring = "year" + askWP.toString() + "C";
		var hoursstring = "hours" + askWP.toString() + "C";
		// var datestring = "date" + askWP.toString() + "C";
		var overhoursstring = "overhours" + askWP.toString() + "C";
		var headmanstring = "headman" + askWP.toString() + "C";
		var myCarstring = "myCar" + askWP.toString() + "C";
		var compCarstring = "compCar" + askWP.toString() + "C";
		var mealstring = "meal" + askWP.toString() + "C";
		
		// Haetaan p�iv�m��r� ja jaotellaan siit� viikonp�iv�, pp,kk ja vv.
		var wkday = weekday[ dayWalker.getDay() ];
		var day = dayWalker.getDate();
		var month = dayWalker.getMonth() + 1; // Kuukaudet numeroina indeksoiden nollasta.
		var year = dayWalker.getFullYear();
		
		// Yhden p�iv�n sis��n laskettavat muuttujat.
		var hours = 0;
		var overhours = 0;
		var headmans = 0;
		var myCars = 0;
		var compCars = 0;
		var meals = 0;
		
		// Selvitet��n p�iv�m��r�� vastaavat tehdyt ty�tunnit:
		// K�yd��n l�pi hourSlips ja haetaan niist� ne, joiden p�iv�m��r� on sama kuin nyt k�sittelyss� oleva eli dayWalker.
		for (var askHS = 0; askHS < hourSlips["rows"].length; askHS++)
		{
		
			// console.log("wagepvm1: " + dayWalker.toString( ) );
			// console.log("slippvm1: " + hourSlips["rows"][askHS]["value"].date );
			if( dayWalker.getTime( ) == hourSlips["rows"][askHS]["value"].date )
			{
				
				// console.log("Edelliset pvmt olivat samat.");
				
				// Lis�t��n t�h�n tuntilappuun liittyv�t muuttujat mukaan.
				
				// console.log("Lis�t��n tavallisia tunteja: " + hourSlips["rows"][askHS]["value"].basicHours );
				
				hours = hours + hourSlips["rows"][askHS]["value"].basicHours;
				
				// console.log("Nyt p�iv�m��r�ll� " + dayWalker.toString() + " on yhteens� " + hours + " tuntia.");
				
				overhours = overhours + hourSlips["rows"][askHS]["value"].overTimeHours100 + hourSlips["rows"][askHS]["value"].overTimeHours50;
				headmans = headmans + hourSlips["rows"][askHS]["value"].headManHours;
				
				if( hourSlips["rows"][askHS]["value"].usedCar == "Oma auto" )
				{
					myCars = myCars + hourSlips["rows"][askHS]["value"].km;
				}
				if( hourSlips["rows"][askHS]["value"].usedCar == "Firman auto" )
				{
					compCars = compCars + hourSlips["rows"][askHS]["value"].km;
				}
				if( hourSlips["rows"][askHS]["value"].meal )
				{
					meals++;
				}
			}
		}
		
		// Lis�t��n yksitt�isen p�iv�n saldot kokonaissummiin.
		allHours = allHours + hours;
		allOverhours = allOverhours + overhours;
		allHeadmans = allHeadmans + headmans;
		allMyCars = allMyCars + myCars;
		allCompCars = allCompCars + compCars;
		allMeals = allMeals + meals;
		
		// Korvataan yksitt�isen p�iv�n tiedot dokkariin.
		HTML = HTML + "\
		<tr>\
			<td id=\"" + wkdaystring + "\">" + wkday + "</td>\
			<td id=\"" + daystring + "\">" + day + "</td>\
			<td id=\"" + monthstring + "\">" + month + "</td>\
			<td id=\"" + yearstring + "\">" + year + "</td>\
			<td id=\"" + hoursstring + "\">" + hours + "</td>\
			<td id=\"" + overhoursstring + "\">" + overhours + "</td>\
			<td id=\"" + headmanstring + "\">" + headmans + "</td>\
			<td id=\"" + myCarstring + "\">" + myCars + "</td>\
			<td id=\"" + compCarstring + "\">" + compCars + "</td>\
			<td id=\"" + mealstring + "\">" + meals + "</td>\
		</tr>";
		
		askWP++;
	}
	
	// Lis�t��n yhteenlaskujen tulokset summasarakkeeseen.
	HTML = HTML + "<tr>\
		<td>-</td>\
		<td>-</td>\
		<td>-</td>\
		<td>Yht</td>\
		<td id=\"allHours\">" + allHours + "</td>\
		<td id=\"allOverhours\">" + allOverhours + "</td>\
		<td id=\"allHeadmans\">" + allHeadmans + "</td>\
		<td id=\"allMyCars\">" + allMyCars + "</td>\
		<td id=\"allCompCars\">" + allCompCars + "</td>\
		<td id=\"allMeals\">" + allMeals + "</td>\
	</tr>";
	
	HTML = HTML + "\
				</tbody>\
			</table>\
		<input value=\"Lataa tiedot\" type=\"button\" onclick=\"$('#combined-dataC').table2CSV()\">\
		</div>";
	console.log("Debug2");
	$('#slipsByClient').replaceWith(HTML);
	console.log("Debug3");
	// $(document).ready(function(){$('#hourSlipsByClient').page("destroy").page();});
	$('#hourSlipsByClient').page("destroy").page();
	console.log("Debug4");
};