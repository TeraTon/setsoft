$(document).ready(function() {
	checkLogin();
	console.log("document ready");
	getClients();
});

function saveClient(){

	console.log("Aloitetaan tilaajadatan l&auml;hetys");
	
	var clientForm = $('#clientForm').serializeArray();
	console.log("formit serialisoitu");

	/*
	LISTAN SIS�LT�:
	0: clientName
	1: clientAdress
	2: clientNumber
	*/
	
	var olio = {};
	console.log("tyhj&auml; olio luotu");
	
	olio["name"] = clientForm[0].value;
	olio["adress"] = clientForm[1].value;
	olio["number"] = clientForm[2].value;
	
	olio["type"] = "client";
	console.log("dokumentin tyypiksi asetettu client");
	
	$.ajax({
		type: 'POST',
		dataType: 'json',
		contentType: "application/json",
		data: JSON.stringify(olio),
		url: "http://54.228.200.239/sansio/",
		success: function(paluuarvo){
			console.log("paluuarvo:");
			console.log(paluuarvo);
			$.mobile.changePage("#savedClients");
			$('#savedClients').page("destroy").page();
		}
	});
};

function getClients(){

	$.ajax({
        dataType: 'json',
 		contentType: "application/json",
 		url: "http://54.228.200.239/sansio/_design/views/_view/clientsByName",
 		success: function(sdata){
			console.log("data received");
     		console.log(sdata);
			injectUsers(sdata);
        }
    });
};

function injectUsers(clients){
	var max = clients["total_rows"];
	var HTML = "<div data-role=\"controlgroup\">";
	var HTMLbegin = "<a href=\"#secureQuestion\" data-transition=\"pop\" data-rel=\"dialog\" data-icon=\"delete\" data-role=\"button\" ";
	var HTMLend = "</a>";
	
	for (var i = 0 ; i < max ; i++ )
	{
		HTML = HTML + HTMLbegin + "onclick=\"dialogInput('" + clients["rows"][i]["value"].name + "','" + clients["rows"][i]["value"]._id + "','" + clients["rows"][i]["value"]._rev + "');return true\">" + clients["rows"][i]["value"].name + HTMLend;
	}
	
	HTML = HTML + "</div>";
	
	$('#listOfSavedClients').replaceWith(HTML);
	$('#savedClients').page("destroy").page();

};

function dialogInput(input,id,rev){
	
	$('#deletedClient').replaceWith("<div id=\"deletedClient\">" + input + "</div>");
	$('#yesButton').replaceWith("<div id=\"yesButton\"><a href=\"#\" data-role=\"button\" data-icon=\"check\" onclick=\"deleteClient('" + id + "','" + rev + "');return true\">Kyll&auml;</a></div>");
};

function deleteClient(id,rev){
	console.log("Deleting URL: http://54.228.200.239/sansio/" + id + "?rev=" + rev);
	$.ajax({
         type: 'DELETE',
         url: "http://54.228.200.239/sansio/" + id + "?rev=" + rev,
         success: function () {
            console.log("Client deleted!");
			$.mobile.changePage("#savedClients");
			$('#savedClients').page("destroy").page();
         }
    });
};