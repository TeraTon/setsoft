var rel = "taplia";

$(document).ready(function() {
	checkLogin();
	console.log("document ready");
	var begin = true;
	getMessages(begin);
});

function saveMessage(forum, header, message, nextID){
	$.mobile.showPageLoadingMsg();
	console.log("Aloitetaan viestin l&auml;hetys");

	/*
	OLIO:
	type: 		"message"							(Type of olio)
	username:	sessionStorage.getItem('username')	(Username of the sender)
	timeCreated:xdate.getTime();					(Time the message was sent)
	header: 	header								(Message header)
	message:	message 							(Whole message)
	forum:		forum 								("taplia", "company" or "instant")
	*/
	
	var olio = {};
	var xdate = new XDate();
	olio["type"] = "message";
	olio["username"] = sessionStorage.getItem('username');
	olio["timeCreated"] = xdate.getTime();
	olio["header"] = header;
	olio["message"] = message;
	olio["forum"] = forum;
	console.log("Otsikko: " + header);
	console.log("Viesti: " + message);
	console.log("Foorumi: " + forum);
	
	$.ajax({
		type: 'POST',
		dataType: 'json',
		contentType: "application/json",
		data: JSON.stringify(olio),
		url: "http://54.228.200.239/sansio/",
		success: function(paluuarvo){
			console.log("paluuarvo:");
			console.log(paluuarvo);
			begin = false;
			getMessages(begin, nextID);
		}
	});
};

function getMessages(begin, nextID){

	$.ajax({
        dataType: 'json',
 		contentType: "application/json",
 		url: "http://54.228.200.239/sansio/_design/views/_view/messagesByHeader",
 		success: function(sdata){
			console.log("data received");
     		console.log(sdata);
			injectMessages(sdata);
			if (!begin){
				$.mobile.changePage(nextID);
				$(nextID).page("destroy").page();
				$.mobile.hidePageLoadingMsg();
			}
        }
    });
};

function injectMessages(messages){
	var max = messages["total_rows"];
	var HTMLBeginTaplia = "<div id=\"listOfTapliaMessages\"><ul data-role=\"listview\" data-filter=\"true\" data-filter-placeholder=\"Etsi viestej&auml;\" data-inset=\"true\">";
	var HTMLBeginCompany = "<div id=\"listOfCompanyMessages\"><ul data-role=\"listview\" data-filter=\"true\" data-filter-placeholder=\"Etsi viestej&auml;\" data-inset=\"true\">";
	var HTMLBeginInstant = "<div id=\"listOfInstantMessages\"><ul data-role=\"listview\" data-filter=\"true\" data-filter-placeholder=\"Etsi viestej&auml;\" data-inset=\"true\">";
	var HTMLTaplia = "";
	var HTMLCompany = "";
	var HTMLInstant = "";
	var HTMLEnd = "</ul></div>";
	
	for (var i = 0 ; i < max ; i++ )
	{
		if (messages["rows"][i]["value"].Forum == "taplia"){
			HTMLTaplia = HTMLTaplia + "<li><a href=\"\" onclick=\"checkMessage(" + i + ");changePage('#readMessage', '#tapliaMessages');return true;\">" + messages["rows"][i]["key"] + "</a></li>";
		}
		else if (messages["rows"][i]["value"].Forum == "company"){
			HTMLCompany = HTMLCompany + "<li><a href=\"\" onclick=\"checkMessage(" + i + ");changePage('#readMessage', '#companyMessages');return true;\">" + messages["rows"][i]["key"] + "</a></li>";
		}
		else{
			HTMLInstant = HTMLInstant + "<li><a href=\"\" onclick=\"checkMessage(" + i + ");changePage('#readMessage', '#instantMessages');return true;\">" + messages["rows"][i]["key"] + "</a></li>";
		}
	}
	
	HTMLTaplia = HTMLBeginTaplia + HTMLTaplia + HTMLEnd;
	HTMLCompany = HTMLBeginCompany + HTMLCompany + HTMLEnd;
	HTMLInstant = HTMLBeginInstant + HTMLInstant + HTMLEnd;
	
	$('#listOfTapliaMessages').replaceWith(HTMLTaplia);
	$('#listOfCompanyMessages').replaceWith(HTMLCompany);
	$('#listOfInstantMessages').replaceWith(HTMLInstant);
};

function checkMessage(row){
	$.ajax({
        dataType: 'json',
 		contentType: "application/json",
 		url: "http://54.228.200.239/sansio/_design/views/_view/messagesByHeader",
 		success: function(messages){
			HTMLHead = "<div id=\"headerCheck\"><h4>" + messages["rows"][row]["key"] + "</h4></div>";
			HTMLMessage = "<div id=\"messageCheck\">" + messages["rows"][row]["value"].Message + "</div>";
			$('#headerCheck').replaceWith(HTMLHead);
			$('#messageCheck').replaceWith(HTMLMessage);
		}
	});
};

function changePage(nextID, prev){
	console.log("Changing page to: " + nextID);
	if (nextID != "null"){
		rel = prev;
		$.mobile.changePage(nextID);
	}
	else{
		$.mobile.changePage(rel);
	}
};