$(document).ready(function() {
	checkLogin();
	console.log("document ready");
	getGigs();
});

function getGigs(){
	$.ajax({
        dataType: 'json',
 		contentType: "application/json",
 		url: "http://54.228.200.239/sansio/_design/views/_view/gigsByClient",
 		success: function(gigs){
			console.log("data received");
			injectGigs(gigs);
        }
    });
};

function injectGigs(gigs){
	var max = gigs["rows"].length;
	var HTMLBegin = "<div id=\"openGigs\">";
	var HTML = "";
	var HTMLEnd = "</div>";
	
	for (var i = 0 ; i < max ; i++ )
	{
		if (gigs["rows"][i]["value"].state == "open"){
			HTML = HTML + "<a href=\"\" data-role=\"button\" onclick=\"checkGig(" + i + ");return true;\">" + gigs["rows"][i]["key"] + ", Avoinna" + "</a>";
		}
		else if (gigs["rows"][i]["value"].state == "started"){
			HTML = HTML + "<a href=\"\" data-role=\"button\" onclick=\"checkGig(" + i + ");return true;\">" + gigs["rows"][i]["key"] + ", Aloitettu" + "</a>";
		}
		else{
			HTML = HTML + "<a href=\"\" data-role=\"button\" onclick=\"checkGig(" + i + ");return true;\">" + gigs["rows"][i]["key"] + ", Valmis" + "</a>";
		}
	}
	
	HTML = HTMLBegin + HTML + HTMLEnd;
	$('#openGigs').replaceWith(HTML);
	$('#gigsPage').page("destroy").page();
};

function saveNewGig(){
	$.mobile.showPageLoadingMsg();
	console.log("Aloitetaan keikan tallennus");
	var lista = $('#newGigForm').serializeArray();	
	var olio = {};

	/*
	OLIO:
	type:			"gig"								(Type of olio)
	username		sessionStorage.getItem('username')	(Username of the creator)
	timeCreated:	xdate.getTime();					(Time the gig was created)
	client: 		lista[0].value						(Gig client)
	work:			lista[1].value						(Gig work)
	state: 			"open"								(State of the gig: Open, Started or Finished)
	*/
	
	var xdate = new XDate();
	olio["type"] = "gig";
	olio["username"] = sessionStorage.getItem('username');
	olio["timeCreated"] = xdate.getTime();
	olio["client"] = lista[0].value;
	olio["work"] = lista[1].value;
	olio["state"] = "open";
	
	$.ajax({
		type: 'POST',
		dataType: 'json',
		contentType: "application/json",
		data: JSON.stringify(olio),
		url: "http://54.228.200.239/sansio/",
		success: function(paluuarvo){
			getGigs();
			$.mobile.changePage("#gigsPage");
			$.mobile.hidePageLoadingMsg();
		}
	});
};

function checkGig(row){
	$.ajax({
        dataType: 'json',
 		contentType: "application/json",
 		url: "http://54.228.200.239/sansio/_design/views/_view/gigsByClient",
 		success: function(gigs){
			console.log("data received");
			var client = gigs["rows"][row]["value"].client;
			var work = gigs["rows"][row]["value"].work;
			var state = gigs["rows"][row]["value"].state;
			$('#checkGigClientHeader').replaceWith("<h1>" + client + "</h1>");
			$('#checkGigClient').replaceWith("<div>Asiakas: " + client + "</div>");
			$('#checkGigWork').replaceWith("<div>Ty&ouml;nkuvaus: " + work + "</div>");
			$('#checkGigState').replaceWith("<div>Ty&ouml;n tila: " + state + "</div>");
			$.mobile.changePage("#checkGigPage");
        }
    });
}