function sendInstantFeedback(header, message, textFieldID){
	$.mobile.showPageLoadingMsg();
	console.log("Aloitetaan pikaviestin l&auml;hetys");

	/*
	OLIO:
	type: 			"message"							(Type of olio)
	username:		sessionStorage.getItem('username')	(User)
	timeCreated:	xdate.getTime();					(Time)
	header: 		header								(Message header)
	message:		message 							(Whole message)
	forum:			"instant"		
	*/
	
	var olio = {};
	var xdate = new XDate();
	olio["type"] = "message";
	olio["username"] = sessionStorage.getItem('username');
	olio["timeCreated"] = xdate.getTime();
	olio["header"] = header;
	olio["message"] = message;
	olio["forum"] = "instant";
	console.log("Otsikko: " + header);
	console.log("Viesti: " + message);
	
	$.ajax({
		type: 'POST',
		dataType: 'json',
		contentType: "application/json",
		data: JSON.stringify(olio),
		url: "http://54.228.200.239/sansio/",
		success: function(paluuarvo){
			console.log("paluuarvo:");
			console.log(paluuarvo);
			$(textFieldID).val('')
			$.mobile.hidePageLoadingMsg();
		}
	});
};