function get_wagePeriods(){

	$.ajax({
        dataType: 'json',
 		contentType: "application/json",
 		url: "http://54.228.200.239/sansio/_design/views/_view/wagePeriodsByFirstDate",
 		success: function(sdata){
			// console.log("data received");
     		// console.log(sdata);
			inject_wagePeriods(sdata);
        }
    });
};

function inject_wagePeriods(wagePeriods){
	var max = wagePeriods["total_rows"];
	var HTML = "";
	var HTMLbegin = "<a href=\"#hourSlips\" data-role=\"button\" rel=\"internal\" onclick=\"getHourSlips(";
	var HTMLmiddle = ");return true;\" >";
	var HTMLend = "</a>";
	
	var wagePeriod = "";
	var firstWeek = "";
	var lastWeek = "";

	for (var i = 0 ; i < max ; i++ )
	{
		var firstDate = new XDate( wagePeriods["rows"][i]["value"].firstDate );
		// console.log("Eka pvm: " + firstDate.toString() );
		// console.log("Eka pvm: " + firstDate );
		firstWeek = firstDate.getWeek();
		
		var lastDate = new XDate( wagePeriods["rows"][i]["value"].lastDate );
		// console.log("Toka pvm: " + lastDate.toString() );
		// console.log("Toka pvm: " + lastDate );
		lastWeek = lastDate.getWeek();
		
		wagePeriod = firstWeek + " - " + lastWeek;
		
		HTML = HTML + HTMLbegin + firstDate + "," + lastDate + HTMLmiddle + wagePeriod + HTMLend;
	}
	
	$('#periods').replaceWith(HTML);
	$('#wagePeriods').page("destroy").page();

};

function sendInstantFeedback(header, message, textFieldID){
	$.mobile.showPageLoadingMsg();
	// console.log("Aloitetaan pikaviestin l&auml;hetys");

	/*
	OLIO:
	type: 		"message"	(Type of olio)
	header: 	header		(Message header)
	message:	message 	(Whole message)
	forum:		"instant"		
	*/
	
	var olio = {};
	olio["type"] = "message";
	olio["header"] = header;
	olio["message"] = message;
	olio["forum"] = "instant";
	// console.log("Otsikko: " + header);
	// console.log("Viesti: " + message);
	
	$.ajax({
		type: 'POST',
		dataType: 'json',
		contentType: "application/json",
		data: JSON.stringify(olio),
		url: "http://54.228.200.239/sansio/",
		success: function(paluuarvo){
			// console.log("paluuarvo:");
			// console.log(paluuarvo);
			$(textFieldID).val('')
			$.mobile.hidePageLoadingMsg();
		}
	});
};