function showUser()
{
	// HTML: <a href="index.html" id="userloggedin" rel="external">Kirjaudu</a>
	console.log( "Käyttäjänä on: " + sessionStorage.getItem('username') );
	if( sessionStorage.getItem('username') !== null )
	{
		$("#userloggedin").replaceWith( "<a href=\"index.html\" id=\"testi\" rel=\"external\">" + sessionStorage.getItem('username') + " (logout)</a>");
	}
};